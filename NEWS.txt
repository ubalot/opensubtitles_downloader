.. This is your project NEWS file which will contain the release notes.
.. Example: http://www.python.org/download/releases/2.6/NEWS.txt
.. The content of this file, along with README.rst, will appear in your
.. project's PyPI page.

News
====

0.2.1
-----

*Release date: 19-Nov-2020*

* Removed 'guessit' library (it was causing verbose output)



0.2.0
-----

*Release date: 18-Nov-2020*

* Added 'guessit' library
* Opensubtitles download doesn't fully trust hash, check that subtitle filename is correct.



0.1.5
-----

*Release date: 07-Feb-2019*

* Fix logging to stdout


0.1.4
-----

*Release date: 07-Feb-2019*

* Improved

